extends Panel

export(NodePath) var resultsPath
onready var results = get_node(resultsPath)

func can_drop_data(position, data):
	return true

func drop_data(position, data):
	data.caller.get_parent().remove_child(data.caller)
	results.set_text(results.text + data.text)
