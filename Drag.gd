extends RichTextLabel

func get_drag_data(position):
	var data = {}
	data.text = self.text
	data.caller = self;
	set_drag_preview(self.duplicate())
	return data
